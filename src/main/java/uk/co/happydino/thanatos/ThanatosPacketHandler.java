package uk.co.happydino.thanatos;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.time.Instant;

public class ThanatosPacketHandler {
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation("thanatos", "main"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

    private static int messageId = 0;
    public static void registerDeathMessageHandler(Consumer<PlayerDeathMessage> handler) {
        INSTANCE.registerMessage(messageId++, PlayerDeathMessage.class, PlayerDeathMessage::encode, PlayerDeathMessage::decode, (msg, ctx) ->  PlayerDeathMessage.handler(msg, ctx, handler));
    }

    public static class PlayerDeathMessage {
        private final String killerId;
        private final String deathSource;
        private final Instant lastDeathInstant;

        public PlayerDeathMessage(String killerId, String deathSource, Instant lastDeathInstant) {
            this.killerId = killerId;
            this.deathSource = deathSource;
            this.lastDeathInstant = lastDeathInstant;
        }

        public static void handler(PlayerDeathMessage message, Supplier<NetworkEvent.Context> ctx, Consumer<PlayerDeathMessage> handler) {
            ctx.get().enqueueWork(() -> {
                if (ctx.get().getDirection().getReceptionSide().isClient()) {
                    handler.accept(message);
                }
            });
            ctx.get().setPacketHandled(true);
        }


        public void encode(PacketBuffer packetBuffer) {
            packetBuffer.writeString(this.killerId);
            packetBuffer.writeString(this.deathSource);
            packetBuffer.writeLong(this.lastDeathInstant.toEpochMilli());
        }

        public static PlayerDeathMessage decode(PacketBuffer packetBuffer) {
            String killerID = packetBuffer.readString();
            String deathSource = packetBuffer.readString();
            Instant lastDeathInstant = Instant.ofEpochMilli(packetBuffer.readLong());
            return new PlayerDeathMessage(killerID,deathSource, lastDeathInstant);
        }

        public String getKillerId() {
            return killerId;
        }

        public String getDeathSource() {
            return deathSource;
        }

        public Instant getLastDeathInstant() {
            return lastDeathInstant;
        }
    }
}
