package uk.co.happydino.thanatos;

import java.util.Random;

public class EntityDetail {

    static <T> T StableChooseFromArray(T[] array, String seed) {
        Random random = new Random(stringToSeed(seed));
        int idx = random.nextInt(array.length);

        return array[idx];
    }

    static long stringToSeed(String s) {
        if (s == null) {
            return 0;
        }
        long hash = 0;
        for (char c : s.toCharArray()) {
            hash = 31L*hash + c;
        }
        return hash;
    }

    public String GetName(String killerId) {
        String[] firstNameList;
        if (GetGender(killerId) == Gender.Male)
        {
            firstNameList = Names.maleFirstNameList;
        }
        else
        {
            firstNameList = Names.femaleFirstNameList;
        }
        String firstName = StableChooseFromArray(firstNameList, killerId + "FirstName");
        String lastName = StableChooseFromArray(Names.surnameList, killerId + "LastName");

        return firstName + " " + lastName;
    }

    public Gender GetGender(String killerId) {
        return StableChooseFromArray(Gender.values(), killerId + "Gender");
    }


    enum Gender {
        Male,
        Female
    }
}
