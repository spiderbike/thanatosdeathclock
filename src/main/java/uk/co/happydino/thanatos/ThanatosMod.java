package uk.co.happydino.thanatos;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.DamageSource;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.fml.network.PacketDistributor;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.core.jmx.Server;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("thanatos")
public class ThanatosMod {
    public static final Logger LOGGER = LogManager.getLogger(ThanatosMod.class);
    private static String TauntMessage;
    private EntityDetail entityDetail = new EntityDetail();
    public ThanatosMod() {
        MinecraftForge.EVENT_BUS.register(this);

        ThanatosPacketHandler.registerDeathMessageHandler(ThanatosMod::onEntityDeathClient);
    }

    public static Instant LastDeathTime = Instant.now();

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent(priority = EventPriority.NORMAL)
    public void eventHandler(RenderGameOverlayEvent event) {
        if (!event.isCancelable() && event.getType() == RenderGameOverlayEvent.ElementType.HELMET) {
            int posX = (event.getWindow().getScaledWidth()) / 2;
            int posY = (event.getWindow().getScaledHeight()) / 2;

            Duration durationSinceLastDeath = Duration.between(LastDeathTime, Instant.now());
            MatrixStack transform = new MatrixStack();
            
            Minecraft.getInstance().fontRenderer.drawString(transform, "Time since last death: " + formatDuration(durationSinceLastDeath), posX + -85,posY + 45, 0xffffff);


            //Only show the taunt for 30 seconds, and if it is set, via a death
            if (durationSinceLastDeath.getSeconds() > 30)
            {
                TauntMessage = null;
            }

            if (TauntMessage != null)
            {
                Minecraft.getInstance().fontRenderer.drawString(transform, TauntMessage, posX + -85,posY + 60, 0xffffff);
            }
        }
    }


    @SubscribeEvent
    public void onEntityDeath(LivingDeathEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof ServerPlayerEntity) {



            ServerPlayerEntity player = (ServerPlayerEntity)entity;

            Entity killerEntity = event.getSource().getTrueSource();
            String damageType;
            String killerId = "";
            if (killerEntity != null && killerEntity.getType().getRegistryName() != null) {
                damageType = killerEntity.getType().getRegistryName().getPath();
                killerId = String.valueOf(killerEntity.getUniqueID());
            } else {
                damageType = event.getSource().getDamageType();
            }




            LOGGER.info(event.getSource());
            ThanatosPacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), new ThanatosPacketHandler.PlayerDeathMessage(killerId, damageType, Instant.now()));
        }

    }

    public static void onEntityDeathClient(ThanatosPacketHandler.PlayerDeathMessage message) {
        LastDeathTime = message.getLastDeathInstant();
        TauntMessage = GenerateTaunt(message);
    }

    public static String GenerateTaunt(ThanatosPacketHandler.PlayerDeathMessage playerDeathMessage)
    {
        String playerName =  "Spiderbike" ; //event.getEntity().getDisplayName().getString();
        switch (playerDeathMessage.getDeathSource()) {
            case "drown":
                return tauntDrown(playerName);
            case "fall":
                return tauntFall(playerName);
        }

        if (StringUtils.isNotEmpty(playerDeathMessage.getKillerId()))
        {
            return tauntMob(playerDeathMessage);
        }
        LOGGER.info("we died because, cause:  "+ playerDeathMessage.getDeathSource());
        return "Have you just given up trying?";
    }

    private static String tauntMob(ThanatosPacketHandler.PlayerDeathMessage playerDeathMessage) {
        EntityDetail entityDetail = new EntityDetail();
        EntityDetail.Gender gender = entityDetail.GetGender(playerDeathMessage.getKillerId());
        return "You were killed by "+ entityDetail.GetName(playerDeathMessage.getKillerId()) + " the "+ playerDeathMessage.getDeathSource();
    }

    private static String tauntFall(String playerName) {
        return "Watch out for that first step. It's a doozy!";
    }

    private static String tauntDrown(String playerName) {

        String  tauntList[] = {
                String.format("they thought they could breathe underwater. %s was wrong", playerName),
                "Do you want some arm bands?",
                "You're so fat you sank."
        };

        return tauntList[getRandom(tauntList.length)];
    }

    public static String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }


    public static int getRandom(int max)
    {
       return (int) (Math.random()*max);
    }

}

